#include "opbkrt.hpp"
#include <fstream>
#include <iostream>

std::ofstream output_file;

bool test_function(opbkrt_node<char>* node)
{
    for(opbkrt_node<char>* solution = node; solution != nullptr; solution = solution->get_next())
    {
        output_file << static_cast<char>(solution->get_value() + 'A');
    }
    output_file << '\n';

    return true;
}

int main()
{
    output_file.open("file.txt");

    opbkrt_manager<char> manager('Z' - 'A' + 1, 3, test_function);
    std::cout << "Backtracking process ended with: " << static_cast<int>(manager.perform_backtrack()) << '\n';
    return 0;
}
