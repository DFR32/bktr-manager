#ifndef OPBKRT_H
#define OPBKRT_H
#include <cstdint>
#include <cassert>
#include <stdexcept>
#include <functional>
#include <cstdlib>

enum RET_SIGNALS
{
    REACHED_END = -1,
    REACHED_CONDITION = 0
};

template <typename T>
class opbkrt_node
{
private:
    // value of this node
    T value;

    // maximum value for the type
    T max_value;

    // pointer to the next node
    opbkrt_node<T>* next;

public:
    opbkrt_node() : value(0), max_value(0), next(nullptr) {}
    ~opbkrt_node() {}

    opbkrt_node(T tmp_value, T tmp_max_value) : value(tmp_value), max_value(tmp_max_value), next(nullptr) {}

    void assign_next(opbkrt_node<T>* tmp_next)
    {
        next = tmp_next;
    }

    T get_value()
    {
        return value;
    }

    T get_max()
    {
        return max_value;
    }

    opbkrt_node<T>* get_next()
    {
        return next;
    }

    bool increment()
    {
        if(++value == max_value)
        {
            value = 0;

            if(next == nullptr)
                return false;

            return next->increment();
        }

        return true;
    }

    template <typename U> friend class opbkrt_manager;
};

template <typename T>
class opbkrt_manager
{

private:
    // information about the limits
    uint64_t values;
    uint64_t positions;

    // container for the nodes
    opbkrt_node<T>* base_node;

    // function to test the solution
    std::function<bool(opbkrt_node<T>*) > test_function_ptr;

public:
    opbkrt_manager() : values(0), positions(0), base_node(nullptr) {}
    ~opbkrt_manager() { std::free(base_node); base_node = nullptr; }

    opbkrt_manager(uint64_t num_values, uint64_t num_positions, std::function<bool(opbkrt_node<T>*) > tmp_test_func_ptr) : values(num_values), positions(num_positions), test_function_ptr(tmp_test_func_ptr)
    {
        if(num_positions <= 1)
            throw std::runtime_error("error: num_positions <= 1");

        base_node = reinterpret_cast<opbkrt_node<T>* >( std::malloc( sizeof(opbkrt_node<T>) * num_positions) );

        // assign the linked-list format to the nodes
        // because we don't always want to iterate the vector
        for(uint64_t i = 0; i < num_positions; ++i)
        {
            base_node[i].value = 0;
            base_node[i].max_value = num_values;

            if( i > 0 )
                base_node[i - 1].next = &base_node[i];
        }

        // just to make sure, set the last one to nullptr
        base_node[num_positions - 1].next = nullptr;
    }

    bool increment_solution()
    {
        if(positions <= 1)
            throw std::runtime_error("error: positions <= 1");

        return base_node->increment();
    }

    opbkrt_node<T>* peek_solution()
    {
        if(positions <= 1)
            throw std::runtime_error("error: positions <= 1");

        return base_node;
    }

    int8_t perform_backtrack()
    {
        // try all possible solutions
        do
        {
            // test for a user-determined end-condition
            if(!test_function(base_node))
                return static_cast<int8_t>(REACHED_CONDITION);

        // go on until we reach the end
        } while(increment_solution() == true);

        // regardless, we have reached the end of the process
        return static_cast<int8_t>(REACHED_END);
    }
};
#endif // OPBKRT_H
